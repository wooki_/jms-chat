package com.rulefinancial.lkjanicki.jmschat;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 */
public class Communication {

	/**
	 * 
	 */
	public static final String SENDER_PROP = "sender";
	private MessageProducer producer;
	private MessageConsumer consumer;
	private Session session;

	public void establishConnection() {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				ActiveMQConnection.DEFAULT_BROKER_URL);
		try {
			Connection connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Topic chatTopic = session.createTopic("chat");
			consumer = session.createConsumer(chatTopic);
			producer = session.createProducer(chatTopic);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String sentTxt, String sender) {
		try {
			TextMessage message = session.createTextMessage(sentTxt);
			message.setStringProperty(SENDER_PROP, sender);
			producer.send(message);
		} catch (JMSException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param listener 
	 * 
	 */
	public void setMessageHandler(MessageListener listener) {
		try {
			consumer.setMessageListener(listener);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
