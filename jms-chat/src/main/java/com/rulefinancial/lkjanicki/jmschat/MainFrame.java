package com.rulefinancial.lkjanicki.jmschat;

import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 * 
 */
public class MainFrame extends JFrame {

	private TextArea chatArea;

	private Communication communication;

	public MainFrame() {
		setResizable(false);
		setSize(new Dimension(400, 350));
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		communication = new Communication();
		communication.establishConnection();
		prepareWindow();
	}

	/**
	 * 
	 */
	private void prepareWindow() {
		add(new JLabel("conversation:"));
		chatArea = new TextArea();
		chatArea.setEditable(false);
		add(chatArea);

		add(new JLabel("your nickname:"));
		final JTextField nickname = new JTextField();
		add(nickname);

		add(new JLabel("your message:"));
		final JTextField message = new JTextField();
		add(message);

		JButton sendButton = new JButton("Send");
		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sentTxt = message.getText();
				message.setText("");
				communication.sendMessage(sentTxt, nickname.getText());
			}
		});
		add(sendButton);

		communication.setMessageHandler(new MessageListener() {
			public void onMessage(Message message) {
				if (message instanceof TextMessage) {
					try {
						String sender = message
								.getStringProperty(Communication.SENDER_PROP);
						chatArea.append("(" + sender + ") "
								+ ((TextMessage) message).getText());
						chatArea.append("\n");
					} catch (JMSException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
	}

}
